package pl.rafalgulinski.java8.examples.interfaces;


public class DefaultMethodExample {
    public static void main(String[] args) {
        A a = new C();
        a.test();
    }
}

class C implements B {
}

interface B extends A {
    default void test() {
        System.out.println("B called");
    }
}

interface A {
    default void test() {
        System.out.println("A called");
    }
}

