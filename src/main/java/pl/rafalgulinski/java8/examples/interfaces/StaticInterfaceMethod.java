package pl.rafalgulinski.java8.examples.interfaces;

import com.google.common.collect.Lists;

import java.util.List;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

public class StaticInterfaceMethod {

    public static void main(String[] args) throws InterruptedException {
        AuditLog.info(">>> Application start");
        final CountDownLatch latch = new CountDownLatch(100);
        ExecutorService executor = Executors.newFixedThreadPool(5);
        submit100Tasks(latch, executor);
        latch.await(5, TimeUnit.SECONDS);
        executor.shutdown();
        AuditLog.info(">>> Application end");
    }

    private static void submit100Tasks(CountDownLatch latch, ExecutorService executor) {
        final Random random = new Random();
        List<Consumer<String>> operations = Lists
                .newArrayList(AuditLog::debug, AuditLog::error, AuditLog::info, AuditLog::trace);
        for (int i = 0; i < 100; i++) {
            final int loopIndex = i;
            executor.execute(() -> {
                int levelIndex = random.nextInt(4);
                operations.get(levelIndex).accept("information from thread no. " + loopIndex);
                latch.countDown();
            });
        }

        Custom c = AuditLog::custom1;
        test(AuditLog::custom1);
        test(AuditLog::custom2);
    }

    private static void test(Custom c) {
        c.log("a", "b");
    }

    @FunctionalInterface
    interface Custom {
        int log(String a, String b);
    }


    static interface AuditLog {

        static int custom1(String message, String format) {
            System.out.println(message);System.out.println(format);
            return 1;
        }

        static int custom2(String message, String format) {
            System.out.println(message);System.out.println(format);
            return 2;
        }

        static void info(String message) {
            System.out.println(LEVEL.INFO.name() + "\t" + message);
        }

        static void debug(String message) {
            System.out.println(LEVEL.DEBUG.name() + "\t" + message);
        }

        static void trace(String message) {
            System.out.println(LEVEL.TRACE.name() + "\t" + message);
        }

        static void error(String message) {
            System.out.println(LEVEL.ERROR.name() + "\t" + message);
        }

        enum LEVEL {
            INFO,
            DEBUG,
            TRACE,
            ERROR;
        }
    }
}
