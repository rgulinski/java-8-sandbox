package pl.rafalgulinski.java8.examples.data;

import org.codehaus.jackson.map.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class Database {
    private ObjectMapper mapper;

    public Database() {
        mapper = new ObjectMapper();
    }

    public List<Company> getCompanies() throws IOException {
        List<Company> companies = mapper.readValue(new File("data.json"),
                mapper.getTypeFactory().constructCollectionType(List.class, Company.class));
        return companies;
    }
}
