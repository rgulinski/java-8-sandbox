package pl.rafalgulinski.java8.examples.data;

import lombok.Data;

import java.util.List;

@Data
public class Department {
    private String name;
    private List<Person> persons;

    public List<Person> getPersons() {
        return persons;
    }

    public String getName() {
        return name;
    }
}
