package pl.rafalgulinski.java8.examples.data;

import lombok.Data;

@Data
public class Address {
    private String country;
    private String city;
    private String details;
}
