package pl.rafalgulinski.java8.examples.data;

import lombok.Data;

@Data
public class Person {
    public String getName() {
        return name;
    }

    private String name;
    private String surname;
    private String jobTitle;
    private int salary;

    public Person() {
    }

    public Person(String name, String surname, String jobTitle, int salary) {
        this.name = name;
        this.surname = surname;
        this.jobTitle = jobTitle;
        this.salary = salary;
    }
}
