package pl.rafalgulinski.java8.examples.data;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PersonGenerator {
    private AtomicInteger personIndex = new AtomicInteger(0);
    private final Random random = new Random();
    private final List<Person> persons;

    public PersonGenerator() {
        List<Person> persons = Lists.newArrayList();
        persons.addAll(generatePersons());
        this.persons = Collections.unmodifiableList(persons);
    }

    private List<Person> generatePersons() {
        final List<String> titles = generateTitles();
        final List<String> names = generateNames();
        return Stream.generate(() -> createPerson(titles, names))
                .limit(names.size()).collect(Collectors.toList());
    }

    public List<Person> getPersons() {
        return persons;
    }

    private static List<String> generateTitles() {
        Set<String> titlePrefix = Sets.newHashSet("Junior", "", "Senior", "Lead");
        Set<String> title = Sets.newHashSet("Developer", "Engineer", "Manager");
         return Sets.cartesianProduct(titlePrefix, title).stream()
                .map(PersonGenerator::merge)
                .collect(Collectors.toList());
    }

    private static List<String> generateNames() {
        Set<String> name = Sets.newHashSet("John", "Blake", "Jack", "Michael", "Brian", "Susan", "Claire", "Steven");
        Set<String> surname = Sets.newHashSet("Kowalsky", "Smith", "Montgomery", "Mountain", "Novak", "Dvorak");
        return Sets.cartesianProduct(name, surname).stream()
                .map(PersonGenerator::merge)
                .collect(Collectors.toList());
    }

    private static String merge(List<String> elements) {
        return elements.stream().reduce("", (acc, s) -> acc + " " + s).trim();
    }

    private Person createPerson(List<String> titles, List<String> names) {
        int index = personIndex.getAndIncrement();
        String title = titles.get(index % titles.size());
        String[] name = names.get(index).split(" ");
        int salary =  calculate(random.nextInt(3000) + 3000, getTitlePrefix(title));
        return new Person(name[0], name[1], title, salary);
    }

    private static int calculate(int base, String title) {
        double factor = 1;
        switch (title) {
            case "Junior":
                factor = 0.8;
                break;
            case "Senior":
                factor = 1.2;
                break;
            case "Lead":
                factor = 1.5;
                break;
            default:
                break;
        }
        return (int)Math.floor(base * factor);
    }

    private static String getTitlePrefix(String title) {
        return title.split(" ")[0];
    }
}
