package pl.rafalgulinski.java8.examples.data;

import lombok.Data;

import java.util.List;

@Data
public class Company {
    private String name;
    private String domain;
    private Address address;
    private List<Department> departments;

    public String getName() {
        return name;
    }

    public String getDomain() {
        return domain;
    }

    public Address getAddress() {
        return address;
    }

    public List<Department> getDepartments() {
        return departments;
    }
}
