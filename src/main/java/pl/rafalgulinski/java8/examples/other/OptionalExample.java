package pl.rafalgulinski.java8.examples.other;

import java.util.Optional;

public class OptionalExample {

    public static void main(String[] args) {
        classicApproach();
        optionalApproach();
    }

    private static void classicApproach() {
        Dao dao = new Dao();
        User user1 = dao.findById(1);
        if (user1 != null) {
            System.out.println(user1.getName());
        }
        User user2 = dao.findById(2);
        // NPE if we forget about that condition
        if (user2 != null) {
            System.out.println(user2.getName());
        }
    }

    private static void optionalApproach() {
        Optional<User> userOptional1 = new DaoDeluxe().findById(1);
        System.out.println(userOptional1.orElse(new User("Guest user")).getName());

        Optional<User> userOptional2 = new DaoDeluxe().findById(2);
        System.out.println(userOptional2.orElse(new User("Guest user")).getName());

        // alternative explicit approach
        if (userOptional2.isPresent()) {
            System.out.println(userOptional2.get().getName());
        }
    }
}

class Dao {
    User findById(int id) {
        return id == 1 ? new User("Administrator") : null;
    }
}

class DaoDeluxe {
    Optional<User> findById(int id) {
        return id == 1 ? Optional.of(new User("Administrator")) : Optional.empty(); // if was not found in database
    }
}

class User {
    String name;
    User(String name) {
        this.name = name;
    }
    String getName() {
        return name;
    }
}
