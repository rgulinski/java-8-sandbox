package pl.rafalgulinski.java8.examples.lambda;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Swing extends JFrame {

    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Swing().setVisible(true);
            }
        });
    }

    private Swing() {
        addComponents();
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        pack();
    }

    private void addComponents() {
        JButton button = new JButton("Ugly button");
//        button.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                JOptionPane.showMessageDialog(null, "hello from ActionListener " + e.getID());
//            }
//        });
        ActionListener al = e -> JOptionPane.showMessageDialog(null, "hello from ActionListener " + e.getID());
        button.addActionListener(al);
        add(button);

    }
}
