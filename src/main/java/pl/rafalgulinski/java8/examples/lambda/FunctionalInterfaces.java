package pl.rafalgulinski.java8.examples.lambda;

import com.google.common.collect.Lists;

import java.util.List;
import java.util.Objects;
import java.util.Stack;
import java.util.function.*;

public class FunctionalInterfaces {

    public static void main(String[] args) {
//        predicateTest();
//        consumerTest();
//        supplierTest();
        unaryTest();
        binaryTest();
    }

    private static void supplierTest() {
        Stack<String> data = new Stack<>();
        data.add("work");
        data.add("it");
        data.add("make");
        data.add("TEST");

        Supplier<String> producer = () -> data.pop();

        while (!data.empty()) {
            String value = producer.get();
            System.out.println(value);
            if (! Objects.equals(value, value.toUpperCase())) {
                data.push(value.toUpperCase());
            }
        }
    }

    private static void predicateTest() {
        List<Integer> integers = Lists.newArrayList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        Predicate<Integer> isEven = x -> x % 2 == 0;
        Predicate<Integer> isOdd = x -> !isEven.test(x);

        for (Integer integer : integers) {
            if (isEven.test(integer)) {
                System.out.println(integer + " is even");
            }
        }
        integers.forEach(x -> {
            if (isEven.test(x)) {
                System.out.println(x);
            }
        });
        for (Integer integer : integers) {
            if (isOdd.test(integer)) {
                System.out.println(integer + " is odd");
            }
        }
    }

    private static void consumerTest() {
        List<String> words = Lists.newArrayList("This", "is", "Sparta");
        Consumer<String> makeUpperAndPrint = w -> System.out.println(w.toUpperCase());
        Consumer<String> printOriginal = w -> System.out.println("Original word: " + w);
        words.forEach(word -> makeUpperAndPrint.andThen(printOriginal).accept(word));
    }

    private static void unaryTest() {
        UnaryOperator<Integer> abs = x -> x >= 0 ? x : -x;
        System.out.println("ABS for -128 is " + abs.apply(-128));
    }

    private static void binaryTest() {
        BinaryOperator<Integer> sum = (x, y) -> x + y;
        System.out.println("Sum of 7 and 8 is " + sum.apply(7, 8));
    }
}

@FunctionalInterface
interface test {
    boolean apply();
    default boolean apply2() {
        return false;
    }
}
