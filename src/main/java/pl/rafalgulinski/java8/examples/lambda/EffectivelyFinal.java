package pl.rafalgulinski.java8.examples.lambda;

import com.google.common.collect.Lists;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class EffectivelyFinal extends JFrame {

    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new EffectivelyFinal().setVisible(true);
            }
        });
    }

    private EffectivelyFinal() {
        addComponents();
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        pack();
    }

    private void addComponents() {
        JButton button = new JButton("Effectively final");
        button.addActionListener(event -> System.out.println("simple lambda action listener"));
        button.addActionListener(event -> {
            System.out.println("Multiline action listener syntax");
            System.out.println("Event id: " + event.getID());
        });

        String message = "You've clicked the button";
        //message = "Button was clicked by you"; // will not compile
        button.addActionListener(event -> System.out.println(message));
        add(button);
    }
}
