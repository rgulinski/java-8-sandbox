package pl.rafalgulinski.java8.examples.streams;

import pl.rafalgulinski.java8.examples.data.Company;
import pl.rafalgulinski.java8.examples.data.Database;

import java.io.IOException;
import java.util.List;

public class FlatmapExample {

    public static void main(String[] args) throws IOException {
        List<Company> companies = new Database().getCompanies();
        printAllDepartments(companies);
    }

    private static void printAllDepartments(List<Company> companies) {
        companies.stream()
                .flatMap(company -> company.getDepartments().stream())
                .flatMap(department -> department.getPersons().stream())
                .forEach(p -> System.out.println(p.getName()));
    }
}
