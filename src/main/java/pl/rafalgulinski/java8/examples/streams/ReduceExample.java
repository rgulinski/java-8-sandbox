package pl.rafalgulinski.java8.examples.streams;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ReduceExample {

    public static void main(String[] args) {
        List<Integer> numbers = Stream.iterate(1, x -> x + 1)
                .peek(data -> System.out.println("BEFORE FILTER \t" + data))
                .filter(x -> x % 2 == 0)
                .peek(data -> System.out.println("AFTER FILTER \t" + data))
                .limit(10)
                .peek(data -> System.out.println("AFTER LIMIT \t" + data))
                .collect(Collectors.toList());
        printSumNumbers(numbers);
    }

    private static void printSumNumbers(List<Integer> numbers) {
        System.out.println("Sum is : " +
                numbers.stream().reduce((acc, x) -> acc + x).get());
    }
}
