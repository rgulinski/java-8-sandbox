package pl.rafalgulinski.java8.examples.streams;

import pl.rafalgulinski.java8.examples.data.Company;
import pl.rafalgulinski.java8.examples.data.Database;

import java.io.IOException;
import java.util.List;
import java.util.stream.Stream;

public class FilterExample {

    public static void main(String[] args) throws IOException {
        List<Company> companies = new Database().getCompanies();
        Stream<Company> companyStream = printSoftwareCompanyNames(companies);

        System.out.println("about to create thread");
        new Thread(() -> companyStream.forEach(System.out::println)).start();
    }

    private static Stream<Company> printSoftwareCompanyNames(List<Company> companies) {
        Stream<Company> firteredCompanies = companies.stream()
                .filter(company -> {
                            System.out.println("filter " + company);
                            return !company.getDomain().toLowerCase().contains("software");
                        })
                .limit(1);
//                .collect(Collectors.toList());
        return firteredCompanies;
//                .forEach(company -> System.out.println(company.getName()));
    }
}
