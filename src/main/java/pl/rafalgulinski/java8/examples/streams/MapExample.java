package pl.rafalgulinski.java8.examples.streams;

import pl.rafalgulinski.java8.examples.data.Company;
import pl.rafalgulinski.java8.examples.data.Database;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MapExample {

    public static void main(String[] args) throws IOException {
        List<Company> companies = new Database().getCompanies();
        gatherCompaniesNames(companies).forEach(System.out::println);

        Map<String, List<Company>> map = companies.stream().collect(Collectors.groupingBy(c -> {
            return c.getName();
        }));
        map.forEach((k, v) -> {
            System.out.println("name " + k);
            System.out.println("companies ");
            v.forEach(System.out::println);
        });
    }

    private static List<String> gatherCompaniesNames(List<Company> companies) {
        return companies.stream()
                .map(company -> company.getName())
                .collect(Collectors.toList());
    }
}
